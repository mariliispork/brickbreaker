/**
* This class holds the information for the brick map.
* 
* Game idea from: http://www.java-online.ch/gamegrid/gamegridEnglish/index.php?inhalt_mitte=pixelgames/breakout.inc.php
* 
* @author  Mari-Liis P�rk ISd 11
*/
package brickbreaker;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class BrickMap {
	// This 2D array will contain the bricks.
	private int map[][];
	private int brickWidth;
	private int brickHeight;

	// Method for declaring how many rows and columns will be generated.
	public BrickMap(int lvl) {
		Levels level = new Levels();
		map = level.levelSelect(lvl);
		brickWidth = 540 / map[0].length;
		brickHeight = 150 / map.length;
	}

	// Function for drawing the bricks.
	public void draw(Graphics2D g) {
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				if (map[i][j] > 0) {

					g.setColor(blockColor(i, j));
					g.fillRect(j * brickWidth + 80, i * brickHeight + 30, brickWidth, brickHeight);
					
					// Sets the border around the bricks.
					g.setStroke(new BasicStroke(3));
					g.setColor(Color.gray.brighter());
					g.drawRect(j * brickWidth + 80, i * brickHeight + 30, brickWidth, brickHeight);
				}

			}
		}
	}

	private Color blockColor(int i, int j) {
		if (map[i][j] == 1) {
			return Color.red.darker();
		}
		if (map[i][j] == 2) {
			return Color.orange.darker();
		}
		if (map[i][j] == 3) {
			return Color.yellow.darker();
		}
		if (map[i][j] == 4) {
			return Color.green.darker();
		}
		if (map[i][j] == 5) {
			return Color.blue;
		}
		if (map[i][j] == 6) {
			return Color.blue.darker();
		}
		if (map[i][j] == 7) {
			return Color.pink.darker();
		} else {
			return Color.blue.darker();
		}
	}
	// Method for setting the brick value.
	public void setBrickValue(int value, int row, int col) {
		map[row][col] = value;
	}
	public int[][] getMap() {
		return map;
	}
	public int getBrickWidth() {
		return brickWidth;
	}
	public int getBrickHeight() {
		return brickHeight;
	}
}
