/**
* This Class file holds the JUnit tests.
* 
* @author  Mari-Liis P�rk ISd 11
*/
package brickbreaker;

import static org.junit.Assert.*;

import javax.swing.JFrame;

import org.junit.Test;

public class GameTest {

	@Test
	public void testMoveRight() {
		JFrame frame = new JFrame();
		GameWindow gamePlay = new GameWindow(frame, 1);
		assertEquals(310, gamePlay.getSlider());
		gamePlay.moveRight();
		assertEquals(330, gamePlay.getSlider());
	}

	@Test
	public void testMoveLeft() {
		JFrame frame = new JFrame();
		GameWindow gamePlay = new GameWindow(frame, 1);
		assertEquals(310, gamePlay.getSlider());
		gamePlay.moveLeft();
		assertEquals(290, gamePlay.getSlider());
	}
	
	@Test
	public void testSetBrickValue() {
		BrickMap brickMap = new BrickMap(1);
		assertEquals(1, brickMap.getMap()[0][0]);
		brickMap.setBrickValue(0, 0, 0);
		assertEquals(0, brickMap.getMap()[0][0]);
	}

}
