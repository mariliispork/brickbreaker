/**
* This Class file holds the information for the game logic and graphics objects.
* 
* Game idea from: http://www.java-online.ch/gamegrid/gamegridEnglish/index.php?inhalt_mitte=pixelgames/breakout.inc.php
* 
* @author  Mari-Liis P�rk ISd 11
*/
package brickbreaker;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;


public class GameWindow extends JPanel implements KeyListener, ActionListener {
	private boolean play = false;
	private boolean pause = false;
	private int score = 0;
	private int totalBricks;
	private Timer timer;
	private int delay = 5;

	// Starting position for the slider.
	private int slider = 310;
	// Starting position for the ball.
	private int ballposX = 120;
	private int ballposY = 350;
	// Sets the direction for the ball.
	private int ballXdir = -1;
	private int ballYdir = -2;
	private int level = 1;
	private int lives = 3;
	private JFrame panelParent;
	
	// Object for the BrickMap class.
	private BrickMap map;

	public GameWindow(final JFrame parent,  int lvl) {
		panelParent = parent;
		level = lvl;
		map = new BrickMap (level);
		totalBricks = countBricks();
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		// Object for the timer.
		timer = new Timer(delay, this);
		timer.start();

	}
	
	// Counts the bricks on the map.
	private int countBricks() {
		int count = 0;
		for (int i = 0; i < map.getMap().length; i++) {
			for (int j = 0; j < map.getMap()[0].length; j++) {
				if (map.getMap()[i][j] > 0) {
					count++;
				}
			}
		}
		return count;
	}
	
	// This function receives the graphics objects.
	public void paint(Graphics g) {
		requestFocus();
		// Background.
		g.setColor(Color.gray.brighter());
		g.fillRect(1, 1, 692, 592);

		// Drawing map(blocks).
		map.draw((Graphics2D) g);

		// Borders for top, left side and right side.
		g.setColor(Color.red.darker());
		g.fillRect(0, 0, 3, 592);
		g.fillRect(0, 0, 692, 3);
		g.fillRect(691, 0, 3, 592);

		// Scores.
		g.setColor(Color.white);
		g.setFont(new Font("aller", Font.BOLD, 25));
		g.drawString("Score: " + score, 10, 30);
		
		// Level.
		g.setColor(Color.white);
		g.setFont(new Font("aller", Font.BOLD, 25));
		g.drawString("Level: " + level, 310, 30);
		
		// Lives.
		g.setColor(Color.white);
		g.setFont(new Font("aller", Font.BOLD, 25));
		g.drawString("Lives: " + lives, 600, 30);

		// The slider.
		g.setColor(Color.blue.darker());
		g.fillRect(slider, 550, 100, 8);

		// The ball.
		g.setColor(Color.blue.darker());
		g.fillOval(ballposX, ballposY, 20, 20);
		
		// In case 0 bricks left, press enter to restart.
		if (totalBricks <= 0) {
			if (play == true) {
				pause = true;
				play = false;
				ballXdir = 0;
				ballYdir = 0;
				level++;
				if (level == 6) {
					level = 1;
				}
			}
			g.setColor(Color.red.darker());
			g.setFont(new Font("aller", Font.BOLD, 30));
			g.drawString("You Won! ", 260, 300);

			g.setFont(new Font("aller", Font.BOLD, 20));
			g.drawString("Press Enter to Restart", 235, 350);

		}
		// In case the ball goes off the screen.
		if (ballposY > 570) {
			if (play == true) {
				pause = true;
				lives--;
				play = false;
				ballXdir = 0;
				ballYdir = 0;
			}
			if (lives < 1){
				g.setColor(Color.red.darker());
				g.setFont(new Font("aller", Font.BOLD, 30));
				g.drawString("Game Over, Total Score: " + score, 182, 300);

				g.setFont(new Font("aller", Font.BOLD, 20));
				g.drawString("Press Enter to Restart", 245, 350);
			}
			if (lives >= 1){
				g.setColor(Color.red.darker());
				g.setFont(new Font("aller", Font.BOLD, 30));
				g.drawString("You Died", 290, 300);

				g.setFont(new Font("aller", Font.BOLD, 20));
				g.drawString("Press Enter to Continue", 245, 350);
			}
		}
		

		g.dispose();

	}

	// Auto-generated methods from the KeyListener and ActionListener.
	@Override
	public void actionPerformed(ActionEvent e) {
		timer.start();
		// Checks if the variable play is true, allows the ball to move.
		if (play) {
			// Detects the slider, creates rectangles around the ball and the paddle.
			if (new Rectangle(ballposX, ballposY, 20, 20).intersects(new Rectangle(slider, 550, 100, 8))) {
				ballYdir = -ballYdir;
			}
				// Accessing the 2D array with the object created inside the class.
				// Used http://stackoverflow.com/questions/12393231/break-statement-inside-two-while-loops.
			A: for (int i = 0; i < map.getMap().length; i++) {
				for (int j = 0; j < map.getMap()[0].length; j++) {
					// Detects if map is > 0 and then detects the intersection.
					if (map.getMap()[i][j] > 0) {
						// Detects the position of the ball and the brick.
						int brickX = j * map.getBrickWidth() + 80;
						int brickY = i * map.getBrickHeight() + 50;
						int brickWidth = map.getBrickWidth();
						int brickHeight = map.getBrickHeight();
						
						// Creates a rectangle around the brick and the ball for detecting the intersection.
						Rectangle rect = new Rectangle(brickX, brickY, brickWidth, brickHeight);
						Rectangle ballRect = new Rectangle(ballposX, ballposY, 20, 20);
						Rectangle brickRect = rect;
						
						// Checks if it intersects or not.
						if (ballRect.intersects(brickRect)) {
							// If it is intersecting call the setBrickValue method.
							map.setBrickValue(0, i, j);
							// Sets total bricks to one less than previous value and adds 5 points to the score.
							totalBricks--;
							score += 5;
							
							
							if (ballposX + 19 <= brickRect.x || ballposX + i >= brickRect.x + brickRect.width) {
								// Moves ball to the opposite direction.
								ballXdir = -ballXdir;
							} else {
								// Moves ball towards the top or bottom direction.
								ballYdir = -ballYdir;
							}
							// Takes it out from the outer loop, label A.
							break A;

						}
					}
				}
			}
			// Detecting if the ball is touching the top left or right corner.
			ballposX += ballXdir;
			ballposY += ballYdir;
			// This is for the left border.
			if (ballposX < 0) {
				ballXdir = -ballXdir;
			}
			// This is for the top side.
			if (ballposY < 0) {
				ballYdir = -ballYdir;
			}
			// This is for the right border.
			if (ballposX > 670) {
				ballXdir = -ballXdir;
			}
		}

		repaint();

	}

	@Override
	// Checks if keys have been pressed.
	public void keyPressed(KeyEvent e) {
		// Checks if the right arrow key is pressed.
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			// Checks that the ball doesen't go outside the panel, if the position is >= 600 keeps it to the panel.
			if (slider >= 600) {
				slider = 600;
			} else if (pause == false){
				moveRight();
			}

		}
		// Checks if left arrow key is pressed.
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			if (slider < 10) {
				slider = 10;
			} else if (pause == false){
				moveLeft();
			}
		}
		// Checks if enter has been pressed, restarts the game if it has.
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			if (!play) {
				setDefaultSettings();
				// This function will redraw everything, calls the paint method again.
				repaint();
			}
		}
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			setVisible(false);
			MenuWindow panel = new MenuWindow(panelParent);
			panelParent.setContentPane(panel);
		}

	}
	// Sets game settings to default.
	private void setDefaultSettings() {
		play = true;
		pause = false;
		ballposX = 120;
		ballposY = 350;
		ballXdir = -1;
		ballYdir = -2;
		slider = 310;
		if (totalBricks <= 0){
			map = new BrickMap(level);
			totalBricks = countBricks();
		}
		if (lives < 1) {
			level = 1;
			score = 0;
			lives = 3;
			map = new BrickMap(level);
			totalBricks = countBricks();
		}
	}
	// Methods for the slider movement.
	public void moveRight() {
		play = true;
		// Right arrow key moves the slider 20 pixels to the right.
		slider += 20;
	}

	public void moveLeft() {
		play = true;
		// Left arrow key moves the slider 20 pixels to the left.
		slider -= 20;
	}
	
	public boolean getPlay() {
		return play;
	}
	
	public int getSlider() {
		return slider;
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
