/**
* This Class file holds the level select information.
* 
* @author  Mari-Liis P�rk ISd 11
*/
package brickbreaker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LevelSelectWindow extends JPanel {
	public LevelSelectWindow(final JFrame parent) {
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		setLayout(null);

		// Title.
		JLabel Title = new JLabel("Level Select");
		Title.setForeground(Color.BLUE);
		Title.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 30));
		Title.setBounds(260, 82, 239, 39);
		add(Title);

		// Levels.
		for (int i = 1; i < 6; i++) {
			JButton startGame = new JButton(Integer.toString(i));
			int level = i;
			startGame.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					setVisible(false);
					GameWindow frame = new GameWindow(parent, level);
					parent.setContentPane(frame);
				}
			});
			startGame.setFont(new Font("aller", Font.BOLD, 12));
			startGame.setBounds(210 + (50 * (i - 1)), 190, 50, 30);
			add(startGame);
		}

		// Back to menu button.
		JButton exitGame = new JButton("Back");
		exitGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				MenuWindow panel = new MenuWindow(parent);
				parent.setContentPane(panel);
			}
		});
		exitGame.setFont(new Font("aller", Font.BOLD, 15));
		exitGame.setBounds(290, 470, 78, 25);
		add(exitGame);
	}
}
