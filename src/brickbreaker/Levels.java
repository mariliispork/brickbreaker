/**
* This Class file holds the level layouts.
* 
* @author  Mari-Liis P�rk ISd 11
*/
package brickbreaker;

public class Levels {
    public int[][] levelSelect(int lvl) {
        switch (lvl) {
            case 1:
                return new int[][] {
                        {1,0,0,0,0,0,0},
                        {1,0,0,0,0,0,0},
                        {0,0,0,0,0,0,0}};
            case 2:
                return new int[][] {
                        {1,2,1,0,1,2,1},
                        {1,0,1,2,1,0,1},
                        {1,2,1,0,1,2,1}};
            case 3:
                return new int[][] {
                        {0,1,1,1,1,1,0},
                        {1,2,1,0,1,2,1},
                        {0,1,1,1,1,1,0}};
            case 4:
                return new int[][] {
                        {1,1,1,2,1,1,1},
                        {2,0,0,0,0,0,2},
                        {1,1,1,2,1,1,1}};
            case 5:
                return new int[][] {
                        {0,0,3,0,5,0,0},
                        {1,0,0,0,0,0,7},
                        {0,2,3,4,5,6,0}};
            default:
                return null;
        }
    }
}
