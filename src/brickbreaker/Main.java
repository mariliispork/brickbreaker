/**
* This program is the BrickBreaker game. 
* The bottom of the screen to move the paddle 
* ball from falling to the bottom of the
* It's a game in which you need to destroy all the bricks in the map.
* 
* Game idea from: http://www.java-online.ch/gamegrid/gamegridEnglish/index.php?inhalt_mitte=pixelgames/breakout.inc.php
*
* @author  Mari-Liis P�rk ISd 11
*/
package brickbreaker;

import javax.swing.JFrame;


public class Main {

	// The main method of the BrickBreaker game. Plays the game and draws the frame.
	public static void main(String[] args) {

		JFrame frame = new JFrame();
		MenuWindow panel = new MenuWindow(frame);
		frame.setBounds(10, 10, 700, 600);
		frame.setTitle("BrickBreaker");
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);
		
		

	}

}
