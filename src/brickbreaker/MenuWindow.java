/**
* This Class file holds the menu window's information and buttons.
* http://docs.oracle.com/javase/tutorial/uiswing/components/menu.html
* 
* @author  Mari-Liis P�rk ISd 11
*/
package brickbreaker;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MenuWindow extends JPanel {
	public MenuWindow(final JFrame parent) {
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		setLayout(null);

		// Title.
		JLabel Title = new JLabel("Brick Breaker");
		Title.setForeground(Color.BLUE);
		Title.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 30));
		Title.setBounds(260, 82, 239, 39);
		add(Title);

		// Start button.
		JButton startGame = new JButton("Start");
		startGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				GameWindow panel = new GameWindow(parent, 1);
				parent.setContentPane(panel);
			}
		});
		startGame.setFont(new Font("aller", Font.BOLD, 15));
		startGame.setBounds(290, 190, 78, 25);
		add(startGame);

		// Level select button.
		JButton levelSelect = new JButton("Level Select");
		levelSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				LevelSelectWindow panel = new LevelSelectWindow(parent);
				parent.setContentPane(panel);
			}
		});
		levelSelect.setFont(new Font("aller", Font.BOLD, 15));
		levelSelect.setBounds(260, 230, 140, 25);
		add(levelSelect);

		// Exit button.
		// http://www.javadocexamples.com/java/awt/event/WindowEvent/WindowEvent.WINDOW_CLOSING.html
		// http://stackoverflow.com/questions/5830256/close-java-frame-using-code
		JButton exitGame = new JButton("Exit");
		exitGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.dispatchEvent(new WindowEvent(parent, WindowEvent.WINDOW_CLOSING));
			}
		});
		exitGame.setFont(new Font("aller", Font.BOLD, 15));
		exitGame.setBounds(290, 270, 78, 25);
		add(exitGame);
	}
}